-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 27, 2019 at 09:40 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heritagemes`
--

-- --------------------------------------------------------

--
-- Table structure for table `articlescientifique`
--

CREATE TABLE `articlescientifique` (
  `id_as` int(11) NOT NULL,
  `titre` varchar(300) NOT NULL,
  `auteur` varchar(300) NOT NULL,
  `annee` year(4) NOT NULL,
  `lien` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articlescientifique`
--

INSERT INTO `articlescientifique` (`id_as`, `titre`, `auteur`, `annee`, `lien`) VALUES
(1, 'The Olympic Games, the quintessential spatial event', 'Augustin', 2009, ' https://doi.org/10.3406/bagf.2009.2675'),
(2, 'L’événement, outil de légitimation de projets urbains', 'Gravari-Barbas et Jacquot', 2007, 'https://journals.openedition.org/geocarrefour/2217'),
(3, '', 'John R. Gold et Margaret M. Gold', 2008, ''),
(4, '', 'Thornley', 2002, ''),
(5, '', 'Ragueneau', 2012, '');

-- --------------------------------------------------------

--
-- Table structure for table `evenementsportif`
--

CREATE TABLE `evenementsportif` (
  `id_es` int(11) NOT NULL,
  `nom` varchar(300) NOT NULL,
  `lieu` varchar(300) NOT NULL,
  `annee` year(4) NOT NULL,
  `lien` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evenementsportif`
--

INSERT INTO `evenementsportif` (`id_es`, `nom`, `lieu`, `annee`, `lien`) VALUES
(1, 'JOete', 'Rome', 1930, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_1960'),
(2, 'JOete', 'Sydney', 2000, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_2000'),
(3, 'JOete', 'Athènes', 2004, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_2004'),
(4, 'JOete', 'Pékin', 2008, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_2008'),
(5, 'JOete', 'Londres', 2012, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_2012'),
(6, 'JOete', 'Rio', 2016, 'https://fr.wikipedia.org/wiki/Jeux_olympiques_d%27%C3%A9t%C3%A9_de_2016');

-- --------------------------------------------------------

--
-- Table structure for table `heritage`
--

CREATE TABLE `heritage` (
  `id_he` int(11) NOT NULL,
  `categorie` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heritage`
--

INSERT INTO `heritage` (`id_he`, `categorie`) VALUES
(1, 'sociale'),
(2, 'economique'),
(3, 'fréquentation touristique'),
(4, 'économie touristique'),
(5, 'image'),
(6, 'aménagement urbain'),
(7, 'sport'),
(8, 'culture');

-- --------------------------------------------------------

--
-- Table structure for table `hypothese`
--

CREATE TABLE `hypothese` (
  `id_hy` int(11) NOT NULL,
  `formulation` text NOT NULL,
  `id_he` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hypothese`
--

INSERT INTO `hypothese` (`id_hy`, `formulation`, `id_he`) VALUES
(1, 'Les ES laissent des traces durables dans le système spatial des territoires hôtes', 6);

-- --------------------------------------------------------

--
-- Table structure for table `sebase`
--

CREATE TABLE `sebase` (
  `id_sb` int(11) NOT NULL,
  `id_as` int(11) NOT NULL,
  `id_hy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sebase`
--

INSERT INTO `sebase` (`id_sb`, `id_as`, `id_hy`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `verifie`
--

CREATE TABLE `verifie` (
  `id_verif` int(11) NOT NULL,
  `id_es` int(11) NOT NULL,
  `id_hy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Une hypothese (id_hy) est verifié par des ES(id_es)';

--
-- Dumping data for table `verifie`
--

INSERT INTO `verifie` (`id_verif`, `id_es`, `id_hy`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articlescientifique`
--
ALTER TABLE `articlescientifique`
  ADD PRIMARY KEY (`id_as`);

--
-- Indexes for table `evenementsportif`
--
ALTER TABLE `evenementsportif`
  ADD PRIMARY KEY (`id_es`);

--
-- Indexes for table `heritage`
--
ALTER TABLE `heritage`
  ADD PRIMARY KEY (`id_he`);

--
-- Indexes for table `hypothese`
--
ALTER TABLE `hypothese`
  ADD PRIMARY KEY (`id_hy`),
  ADD KEY `lienHyHe` (`id_he`);

--
-- Indexes for table `sebase`
--
ALTER TABLE `sebase`
  ADD PRIMARY KEY (`id_sb`),
  ADD KEY `lienSbHy` (`id_hy`),
  ADD KEY `lienSbAs` (`id_as`);

--
-- Indexes for table `verifie`
--
ALTER TABLE `verifie`
  ADD PRIMARY KEY (`id_verif`),
  ADD KEY `lienVeHy` (`id_hy`),
  ADD KEY `lienVeEs` (`id_es`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articlescientifique`
--
ALTER TABLE `articlescientifique`
  MODIFY `id_as` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `evenementsportif`
--
ALTER TABLE `evenementsportif`
  MODIFY `id_es` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `heritage`
--
ALTER TABLE `heritage`
  MODIFY `id_he` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hypothese`
--
ALTER TABLE `hypothese`
  MODIFY `id_hy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sebase`
--
ALTER TABLE `sebase`
  MODIFY `id_sb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `verifie`
--
ALTER TABLE `verifie`
  MODIFY `id_verif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hypothese`
--
ALTER TABLE `hypothese`
  ADD CONSTRAINT `lienHyHe` FOREIGN KEY (`id_he`) REFERENCES `heritage` (`id_he`);

--
-- Constraints for table `sebase`
--
ALTER TABLE `sebase`
  ADD CONSTRAINT `lienSbAs` FOREIGN KEY (`id_as`) REFERENCES `articlescientifique` (`id_as`),
  ADD CONSTRAINT `lienSbHy` FOREIGN KEY (`id_hy`) REFERENCES `hypothese` (`id_hy`);

--
-- Constraints for table `verifie`
--
ALTER TABLE `verifie`
  ADD CONSTRAINT `lienVeEs` FOREIGN KEY (`id_es`) REFERENCES `evenementsportif` (`id_es`),
  ADD CONSTRAINT `lienVeHy` FOREIGN KEY (`id_hy`) REFERENCES `hypothese` (`id_hy`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
