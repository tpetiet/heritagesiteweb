<?php
function liste_Hypo($link){
  $requetels = "SELECT id_hy,formulation FROM hypothese";
  $out = "<form method='get'><select name='hypoSelect'>";
  if ($resultatls = mysqli_query($link,$requetels)){
    while($ligne=mysqli_fetch_assoc($resultatls)){
      $out .="<option value=".$ligne['id_hy'].">".$ligne["formulation"]."</option>";
    }
    $out .="</select><input type='submit' value='OK'></form>";
  }
  return $out;
}

function liste_Heritage($link){
  $requeteHe = "SELECT id_he,categorie FROM heritage";
  $out = "<select name='HeritSelect'>";
  if ($resultatHe = mysqli_query($link,$requeteHe)){
    while($ligne=mysqli_fetch_assoc($resultatHe)){
      $out .="<option value=".$ligne['id_he'].">".$ligne["categorie"]."</option>";
    }
    $out .="</select>";
  }
  return $out;
}

function liste_Articles($link){
  $requetelsAS = "SELECT id_as,auteur,annee FROM articlescientifique";
  $out = "<div id='Div_lstAS'>";
  if ($resultatlsAS = mysqli_query($link,$requetelsAS)){
    while($ligne=mysqli_fetch_assoc($resultatlsAS)){
      $out .="<p><input type='checkbox' name='lstAS[]' value=".$ligne["id_as"]."><label>".$ligne["auteur"].", ".$ligne["annee"]."</label></p>";
    }
    $out .="</div>";
  }
  return $out;
}

function liste_Evenements($link){
  $requetelsES = "SELECT id_es,nom,lieu,annee FROM evenementsportif";
  $out = "<div id='Div_lstES'>";
  if ($resultatlsES = mysqli_query($link,$requetelsES)){
    while($ligne=mysqli_fetch_assoc($resultatlsES)){
      $out .="<p><input type='checkbox' name='lstES[]' value=".$ligne["id_es"]."><label>".$ligne["nom"].", ".$ligne["lieu"].", ".$ligne["annee"]."</label></p>";
    }
    $out .="</div>";
  }
  return $out;
}

function donne_Articles($link,$hypothese){
  $requeteAS = "SELECT artsc.*
  FROM articlescientifique AS artsc, sebase AS sb, hypothese AS hy
  WHERE (artsc.id_as = sb.id_as AND sb.id_hy = hy.id_hy AND hy.id_hy=$hypothese)";
  $tabAS = [];
  if ($resultatAS = mysqli_query($link,$requeteAS)){
    while($ligne=mysqli_fetch_assoc($resultatAS)){
      $tabAS[] = $ligne;
    }
  }
  return $tabAS;
}

function donne_Evenements($link,$hypothese){
  $requeteES = "SELECT es.*
  FROM evenementsportif AS es, verifie AS ve, hypothese AS hy
  WHERE (es.id_es = ve.id_es AND ve.id_hy = hy.id_hy AND hy.id_hy = $hypothese)";
  $tabES = [];
  if ($resultatES = mysqli_query($link,$requeteES)){
    while($ligne=mysqli_fetch_assoc($resultatES)){
      $tabES[] = $ligne;
    }
  }
  return $tabES;
}

function donne_hypo($link,$hypothese){
  $requeteHy = "SELECT hy.formulation
  FROM hypothese AS hy
  WHERE (hy.id_hy = $hypothese)";
  $h = "";
  if ($resultatHy = mysqli_query($link,$requeteHy)){
    while($ligne=mysqli_fetch_assoc($resultatHy)){
      $h = $ligne["formulation"];
    }
  }
  return $h;
}

function afficheDonnee($tab){
  $out = "<table>";
  foreach ($tab as $tab_assoc) {
    $out .="<tr>";
    foreach ($tab_assoc as $key => $elem) {
      if ($key == "id_as" OR $key == "id_es"){
        $out .="";
      }
      elseif ($key == "lien"){
        $out .="<td><a href=".$elem.">".$elem."</a></td>";
      }
      else{
        $out .="<td>".$elem."</td>";
      }
      //echo $key . " : " . $elem;
    }
    $out .="</tr>";
  }
  $out .="</table>";
  return $out;
}

function creerHypoBDD($link,$nom,$id_heritage){
  $newHypo = "INSERT INTO `hypothese`(`id_hy`, `formulation`, `id_he`) VALUES (NULL,'$nom',$id_heritage)";
  if (mysqli_query($link,$newHypo)){
    $findID = "SELECT id_hy FROM hypothese WHERE formulation='".$nom."'";
    if ($result = mysqli_query($link,$findID)){
      $ligne = mysqli_fetch_assoc($result);
      $id = $ligne["id_hy"];
      return $id;
    }
  }
}

function creerLienAS($link,$ASref,$id_hypo){
  foreach ($ASref as $key => $value) {
    $lienAS = "INSERT INTO `sebase`(`id_sb`, `id_as`, `id_hy`) VALUES (NULL,$value,$id_hypo)";
    mysqli_query($link,$lienAS);
  }
}

function creerLienES($link,$ESref,$id_hypo){
  foreach ($ESref as $key => $value) {
    $lienES = "INSERT INTO `verifie`(`id_verif`, `id_es`, `id_hy`) VALUES (NULL,$value,$id_hypo)";
    mysqli_query($link,$lienES);
  }
}

function AjouterHypotheses($link){
  $nom = $_POST["formulation"];
  $id_heritage = $_POST["HeritSelect"];
  $ASref = $_POST["lstAS"];
  $ESref = $_POST["lstES"];
  $id_hypothese = creerHypoBDD($link,$nom,$id_heritage);
  creerLienAS($link,$ASref,$id_hypothese);
  creerLienES($link,$ESref,$id_hypothese);
}

 ?>
