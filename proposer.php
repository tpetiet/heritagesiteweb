<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Site de partage d'hypothèse sur les MES</title>
    <link rel="stylesheet" href= "style.css">
    <link rel="stylesheet" href="site.php" />
  </head>
  <body>
    <nav id="navigateur">
      <ul>
        <li><a href="index.php">Accueil</a></li><!--
     --><li><a href="visionner.php">Visionner</a></li><!--
     --><li><a href="proposer.php">Proposer</a></li><!--
     --><li> <a href="production.html">Production</a></li>
      </ul>
    </nav>

    <?php
    $link = mysqli_connect("localhost", "root", "root", "heritagemes");
    mysqli_set_charset($link, "utf8");
    include("fonction.php");
    $lstHerit = liste_Heritage($link);
    $lstAS = liste_Articles($link);
    $lstES = liste_Evenements($link);

    if (isset($_POST["formulation"])){
      AjouterHypotheses($link);
    }
    ?>

    <div id="corps">
      <div id="proposer">
        <h2>Ajouter une hypothèse</h2>
        <form name="ajoute" action="" method="post">
          <p><label>Formuler votre hypothèse <input type="text" name="formulation"></label></p>
          <p><label>Selection le type d'héritage <?php echo $lstHerit ?></label></p>
          <p>Sur quels articles se basent votre hypothèse? <?php echo $lstAS ?></p>
          <p>Quel événements verifient cette hypothèse <?php echo $lstES ?></p>
          <p><input type="submit" value="OK"></p>
        </form>
      </div>
    </div>
  </body>
</html>
