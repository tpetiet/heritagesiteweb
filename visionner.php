<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Site de partage d'hypothèse sur les MES</title>
    <link rel="stylesheet" href= "style.css">
    <link rel="stylesheet" href="site.php" />
  </head>
  <body>
    <nav id="navigateur">
      <ul>
        <li><a href="index.php">Accueil</a></li><!--
     --><li><a href="visionner.php">Visionner</a></li><!--
     --><li><a href="proposer.php">Proposer</a></li><!--
     --><li> <a href="production.html">Production</a></li>
      </ul>
    </nav>

    <?php
    $link = mysqli_connect("localhost", "root", "root", "heritagemes");
    mysqli_set_charset($link, "utf8");
    include("fonction.php");
    ?>

    <div id="corps">
      <h1 id="visionner">Visionner une hypothèse</h2>
      <p>En cliquant sur l'hyphothèse de votre choix la liste des articles sur lequel s'est basé l'hypothèse est fourni ainsi que la liste des MES étudiés.</p>

      <?php
      $lstHypo = liste_Hypo($link);

      echo $lstHypo;
      if(isset($_GET["hypoSelect"])){
        $hypothese = $_GET["hypoSelect"];
        $tabAS = donne_Articles($link,$hypothese);
        $tabES = donne_Evenements($link,$hypothese);
        $form = donne_hypo($link,$hypothese);
      }
      ?>

      <div id="VisionnerHypotheses">
        <h3><?php echo $form ?></h2>

        <h4>Liste des articles sur lesquels se base l'hypothèse :</h4>
        <ul>
          <?php
          $afficheAS = afficheDonnee($tabAS);
          echo $afficheAS ?>
        </ul>

        <h4>Liste des événements qui verifient cette hypothèse :</h4>
        <ul>
          <?php
          $afficheES = afficheDonnee($tabES);
          echo $afficheES ?>
        </ul>
      </div>
    </div>
  </body>
</html>
